import FastFurierTransform
import numpy as np
import decimal
import matplotlib.pyplot as plt

def drange(x, y):
    jump = (y-x)/1024
    while x < y:
        yield float(x)
        x += decimal.Decimal(jump)


def normaldist(mean, std, x):
    return (1/(np.sqrt(2*np.pi*std)))*np.exp((-1/(2*std*std))*np.power(x-mean,2))

mu, sigma = 0, 1  # mean and standard deviation
x = np.array(list(drange(-4, 4)))
s = normaldist(mu, sigma, x)

f = FastFurierTransform.FastFurierTransform()
rta = f.furierRealNum(s, 1)

plt.figure(1)
plt.plot(rta, c='blue')
plt.grid(True)
plt.show()