import numpy as np


class FastFurierTransform:
    def furierRealNum(self, data, isign):
        """Replaces data[0..2*n-1] by its discrete Fourier transform, if isign is input as 1; or replaces
        data[0..2*n-1] by n times its inverse discrete Fourier transform, if isign is input as 1. data
        is a real array of length n. n must be an integer power of 2.
        """
        idata = np.zeros(data.size*2)
        for i in range(0, data.size):
            idata[i * 2] = data[i]  # asign real part equal to real index
            idata[i * 2 + 1] = 0;  # asign imaginary part equal to zero
        idata = self.furier(idata, isign)

        #Reverse complex data to real
        rdata = np.zeros(data.size)
        for i in range(0, data.size, 1):
            rdata[i]=idata[i * 2];
        return rdata;

    def furier(self, data, isign):
        """Replaces data[0..2*n-1] by its discrete Fourier transform, if isign is input as 1; or replaces
        data[0..2*n-1] by n times its inverse discrete Fourier transform, if isign is input as 1. data
        is a complex array of length n stored as a real array of length 2*n. n must be an integer power of 2.
        """
        n = int(data.size / 2)

        if n < 2 or n & (n - 1):
            raise DataLengthError("n must be power of 2 in four1")

        nn = n << 1
        j = 1
        for i in range(1, nn-1, 2):
            if j > i:
                data[i - 1], data[j - 1] = self.swap(data[j - 1], data[i - 1])
                data[i], data[j] = self.swap(data[j], data[i])
            m = n
            while m >= 2 and j > m:
                j -= m
                m >>= 1
            j += m
        # Here begins the Danielson-Lanczos section of the routine.
        mmax = 2
        while nn > mmax:  # Outer loop executed log2 n times.
            istep = mmax << 1
            theta = isign * (6.28318530717959 / mmax)  # Initialize the trigonometric recurrence.
            wtemp = np.sin(0.5 * theta)
            wpr = -2.0 * wtemp * wtemp
            wpi = np.sin(theta)
            wr = 1.0
            wi = 0.0
            for m in range(1, mmax - 1, 2):
                for i in range(m, nn, istep):
                    # This is the Danielson-Lanczos formula
                    j = i + mmax
                    tempr = wr * data[j - 1] - wi * data[j]
                    tempi = wr * data[j] + wi * data[j - 1]
                    data[j - 1] = data[i - 1] - tempr
                    data[j] = data[i] - tempi
                    data[i - 1] += tempr
                    data[i] += tempi
                wtemp = wr
                wr += wtemp * wpr - wi * wpi  # Trigonometric recurrence.
                wi += wi * wpr + wtemp * wpi
            mmax = istep
        return data;

    @staticmethod
    def swap(x1, x2):
        return x2, x1


class Error(Exception):
    """Base class for exceptions in this module."""
    pass


class DataLengthError(Error):
    """Exception raised for errors in the input data.

    Attributes:
        expression -- input expression in which the error occurred
        message -- explanation of the error
    """

    def __init__(self, message):
        self.message = message
